# Random avatar generator - Identico

## About

-Note : This was a fun project I did when learning. This is not ideal way to generate random avatars and should not be used in any serious applications.

Le projet "Random Avatar Generator - Identicon", également connu sous le nom d'Identicon, est une application développé en utilisant PHP8, CSS, HTML et JavaScript qui génère des avatars visuels uniques à partir d'une chaîne de caractères donnée. Lorsqu'une chaîne de caractères est générée, l'application utilise l'algorithme de hachage MD5 pour créer un hash unique associé à cette chaîne de caractères. Ensuite, elle utilise ce hash pour créer un avatar en utilisant une grille de 6x6 cases, chaque case étant remplie ou vide en fonction de la valeur du hash. Enfin, l'avatar généré est converti en un format de données image png et renvoyé au client sous forme de données binaires encodées en base64. L'application est également dotée d'une fonction de génération de chaînes de caractères aléatoires, qui peut être utilisée pour créer des avatars aléatoires en effectuant une requête (Une API simple a été implémentée).

"I created the Identicon avatar generator using PHP8, CSS, HTML, and JavaScript and also implemented a simple API. This generator produces random avatars, but it is not always able to generate funnys ones..."

## Avatars examples
![view](assets/AvatarsExamples.png)

## API , Code example

```
fetch('https://ovsmar.alwaysdata.net/api/api.php')
.then(function(response) {
  response.json().then(jsonData => {
    console.log(jsonData);   
});
})

```
- [For more example](https://gitlab.com/ovsmar/random-avatar-generator/-/tree/main/test-API)

### The API returns a response in json format;
{
  "id": "USfrpYYiCD",
  "avatar": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUg...
}
![view](assets/result.png)

**links**

- [Random avatar generator - Identicon](https://ovsmar.alwaysdata.net/)
- [API LIVE TEST](https://ovsmar.alwaysdata.net/test-API/)
- [API](https://ovsmar.alwaysdata.net/api/api.php)
