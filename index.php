<?php
//function pour générer avatar aléatoire
function generateIdenticon($string, $size = 200, $margin = 18) {
  // Pour convertir la chaîne en une valeur de hachage
  $hash = md5($string);

  // Pour créer une image avec la taille spécifiée, en tenant compte de la margine
  $image = imagecreatetruecolor($size + 2 * $margin, $size + 2 * $margin);

  // Pour générer des couleurs aléatoires pour l'arrière-plan et le premier plan
  // $bgColor = imagecolorallocate($image, rand(0, 255), rand(0, 255), rand(0, 255));
  $bgColor = imagecolorallocate($image, 240, 240, 240);
  $fgColor = imagecolorallocate($image, rand(0, 255), rand(0, 255), rand(0, 255));

  // Pour remplir l'image avec la couleur de fond
  imagefill($image, 0, 0, $bgColor);

  // Pour définition de la taille de la cellule en fonction de la taille de l'image
  $cellSize = ($size + 0 * $margin) / 6 ;

  // Boucle sur le hachage et dessine un rectangle pour chaque caractère.
  for ($i = 0; $i < strlen($hash); $i++) {
    // Calculer la ligne et la colonne pour le caractère courant.
    $row = $i % 6;
    $col = floor($i / 6);

    // Calculer les positions x et y 
    $x = $col * $cellSize + $margin;
    $y = $row * $cellSize + $margin;

    // Pour convertir le caractère en un nombre entier
    $charInt = intval($hash[$i], 16);

    // Pour vérifie si le caractère est pair ou impair.
    if ($charInt % 2 == 0) {
      // Dessinez un rectangle avec la couleur de premier plan si le caractère est pair.
      imagefilledrectangle($image, $x, $y, $x + $cellSize, $y + $cellSize, $fgColor);
    }
  }
// Pour envoie la image PNG vers un navigateur ou un fichier
  imagepng($image);
  //Pour Lir le contenu courant du tampon de sortie puis l'efface
  $imgData=ob_get_clean();
  // Pour libèrer toute la mémoire associée à l'image
  imagedestroy($image);

  //pour afficher l'image 
    echo '<a id="download-link" href="data:image/png;base64,'.base64_encode($imgData).'" download="image.png"><img src="data:image/png;base64,'.base64_encode($imgData).'" /></a>';

// button pour générer avatar
    echo "<form method='post' action='index.php' name='edit'>
            <input  name='generate' value='Generate' type='submit'>  
          </form>
          ";
//Pour télécharger avatar en cliquant
    echo '<a id="download-link" href="data:image/png;base64,'.base64_encode($imgData).'" download="avatar.png">Dowland</a>';
    
     //  script qui permet télécharger avatar en cliquant sur image/button
          echo"
          <script>
  document.getElementById('download-link').addEventListener('click', function(event) {
    // Prevent the default link behavior
    event.preventDefault();

    // Download the image
    window.open(this.href = this.href.replace(/^data:image\/[^;]/, 'data:application/octet-stream'))
  });
</script>";


echo"
<style>
body {
 margin: 0;
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;
}
input{
  transform: translate(91%, -46%);
}


</style>";
    
  }

  // fonction pour générer une chaîne aléatoire
  function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
  // appeler la fonction generateIdenticon pour générer l'avatar avec l'argument de la fonction pour générer une chaîne aléatoire. 
  generateIdenticon(generateRandomString());


