<?php


function generateIdenticonAPI($string, $size = 200, $margin = 18) {
    
    $hash = md5($string);

    $image = imagecreatetruecolor($size + 2 * $margin, $size + 2 * $margin);
  
    $bgColor = imagecolorallocate($image, 240, 240, 240);
    $fgColor = imagecolorallocate($image, rand(0, 255), rand(0, 255), rand(0, 255));
  
    imagefill($image, 0, 0, $bgColor);
  
    $cellSize = ($size + 0 * $margin) / 6 ;
  
    for ($i = 0; $i < strlen($hash); $i++) {

      $row = $i % 6;
      $col = floor($i / 6);
  
      $x = $col * $cellSize + $margin;
      $y = $row * $cellSize + $margin;
  
      $charInt = intval($hash[$i], 16);
  
      if ($charInt % 2 == 0) {
   
        imagefilledrectangle($image, $x, $y, $x + $cellSize, $y + $cellSize, $fgColor);
      }
    }
 
    imagepng($image);
    $imgData=ob_get_clean();
    imagedestroy($image);
  
    header("Content-Type: application/json");
    header('Access-Control-Allow-Origin: *');
    
    $data = array(
        'id' => generateRandomString(),
        'avatar'=> 'data:image/png;base64,'.base64_encode($imgData).''
    );
    
    $json = json_encode($data);
    if ($json === false) {
      
        $json = json_encode(["jsonError" => json_last_error_msg()]);
        if ($json === false) {
       
            $json = '{"jsonError":"unknown"}';
        }

        http_response_code(500);
    }
    echo $json;
      
    }

    function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }
    
  generateIdenticonAPI(generateRandomString());
  
  
?>

 